# _Chucknorris Challange Plan_

## Steps:

## - Repository initialization

- Initialisate a new blank git repository and setup branches with their rules

## - Project initialization

- Initialisate a new React app that compile through Webpack
- Setup the application to compile SCSS
- Setup folders architecture
- Setup the store where we will keep the data
- Write a TSLint config file

## - Template Integration

- Setup routes and blank pages
- Declare SCSS variables
- Start integrating the tepmlate
- Adding responsive layout

## - API Consumption

- Setup the api call that will fetch the data
- Parse the data to stored with a way which easily allows to get them for by their specific category
- Integrate the pagination
- Adding the search and filters if existe
- Handle the single joke page case

## - Tests

- Creating a unit tests for most shared elements
- Creating integration that try to cover all scenarios
- Creating end to end test

#

#

## License

Sofiene DIMASSI

**dimessisofiene@gmail.com**
