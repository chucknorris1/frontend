import React from "react";
import Route from './routes/Route';

const App = () => {
    return <div className="App">
        <Route />
    </div>;
};

export default App;