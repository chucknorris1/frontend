import React from 'react';
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import Dashboard from "../pages/Dashboard/Dashboard";
import { PATH_DASHBOARD } from "./Paths";

function MainRoute() {

  return (
    <BrowserRouter>
      <Routes>
        <Route exact path={PATH_DASHBOARD} element={<Dashboard />}></Route>
        <Route path="*" element={<Navigate to={PATH_DASHBOARD} />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default MainRoute;
